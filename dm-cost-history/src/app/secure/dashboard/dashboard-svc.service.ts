import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';

import {  IS_API, HTTP_OPTIONS,ITEM_LIST_URL,COST_COUNT_LIST_URL,COST_UNIT_LIST_URL } from './../../common/url-config';
import {Item} from './Item';


@Injectable(
//  {  providedIn: 'root'}
)
export class DashboardSvcService {

    constructor(private httpClient: HttpClient) {

    }
    errorHandler(error: HttpErrorResponse) {
      console.log(JSON.stringify(error));
      return throwError("Server error ...");
    }

    /** Api hit for item lists **/
    getItemList(): Observable<any> {
      console.log("item List Url", ITEM_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(ITEM_LIST_URL , HTTP_OPTIONS)
          .pipe(  catchError(this.errorHandler) );
      } else {
        return this.httpClient.get<any[]>(ITEM_LIST_URL).pipe(
          catchError(this.errorHandler)
        );
      }
    }

    /** Api hit for item lists **/
/*    getItem1(itemId): Observable<any> {

      console.log("item List Url", ITEM_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(ITEM_LIST_URL , HTTP_OPTIONS)
          .pipe(
            catchError(this.errorHandler)
          );
      } else {
        return this.httpClient.get<any[]>(ITEM_LIST_URL)
        .pipe(
          map(result=>{
            let itemData =  this.filterItem(result,itemId);
            return itemData;
          }),
          catchError(this.errorHandler)
        );
      }
    } */

    /** Api hit for item detail **/
    getItem(itemId): Observable<Item> {

        return this.httpClient.get<any>(ITEM_LIST_URL)
        .pipe(
          map(result=>{
            let itemData:Item =  this.filterItem(<Item[]>result.items,itemId);
            return itemData;
          }),
          catchError(this.errorHandler)
        );
    }

    // to get the details of the selected item from the item list
    filterItem(itemList:Item[],itemId:string){
        let items:Item[] = []; let item = new Item;
        items = itemList.filter(item=>  item.retailitemnumber === itemId);
        if(items[0] !== undefined){
           item = item.convertItem(items[0]);
         }
        return item;
    }

    /** API for item list suggestions based on search string **/
    getItemsById(searchText): Observable<any> {

      console.log("item List Url", ITEM_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(ITEM_LIST_URL , HTTP_OPTIONS)
          .pipe(   catchError(this.errorHandler)  );
      } else {
        return this.httpClient.get<any[]>(ITEM_LIST_URL)
        .pipe(
            map(result=>{
              let itemData =  this.filterSearchItems(result,searchText);
              return itemData;
            }),
            catchError(this.errorHandler)
        );
      }
    }


    filterSearchItems(itemList,itemId){
      let items = []; let item = {};
      items = itemList.items.filter(item=>  item.retailitemnumber.includes(itemId)).map(item=>item.retailitemnumber);
      return items;

    //  items = itemList.items.filter(item=>  item.retailitemnumber.includes(itemId)).map(item=>  item.retailitemnumber);
    //  items=  items.map(item=>item.retailitemnumber);
    }
        //
    /** Api hit for item cost change count **/
    getCostChangeCount(): Observable<any> {

      console.log("COST_COUNT_LIST_URL ", COST_COUNT_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(COST_COUNT_LIST_URL , HTTP_OPTIONS)
          .pipe(  catchError(this.errorHandler)   );
      } else {
        return this.httpClient.get<any[]>(COST_COUNT_LIST_URL).pipe(
          catchError(this.errorHandler)
        );
      }
    }

    /** Api hit for item cost change count **/
    getCostChangeCountByItem(itemId): Observable<any> {

      console.log("COST_COUNT_LIST_URL ", COST_COUNT_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(COST_COUNT_LIST_URL , HTTP_OPTIONS)
          .pipe(   catchError(this.errorHandler)  );
      } else {
        return this.httpClient.get<any[]>(COST_COUNT_LIST_URL).pipe(
          map(result=>{
            let itemData =  this.filterItemCostChangeSeries(result,itemId);
            return itemData;
          }),
          catchError(this.errorHandler)
        );
      }
    }


    filterItemCostChangeSeries(sList,itemId){
      let filteredItemCostCount = sList.filter(
          (costYear) =>   costYear.series.some((costItem) => costItem.name === itemId)
          ).map( costYear =>
               {
                  let newElt = Object.assign({},
                                      costYear,
                                      {'series':costYear.series.filter(  costItem => costItem.name === itemId )}
                                    ); // copies costYear
                  return newElt;
                });

        return filteredItemCostCount;
    }

    //
    //
    /** Api hit for item cost change unit **/
    getCostChangeUnit(): Observable<any> {

      console.log("COST_UNIT_LIST_URL ", COST_UNIT_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(COST_UNIT_LIST_URL , HTTP_OPTIONS)
          .pipe( catchError(this.errorHandler)  );
      } else {
        return this.httpClient.get<any[]>(COST_UNIT_LIST_URL).pipe(
          catchError(this.errorHandler)
        );
      }
    }
    //
}
