import { TestBed } from '@angular/core/testing';

import { DashboardSvcService } from './dashboard-svc.service';

describe('DashboardSvcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardSvcService = TestBed.get(DashboardSvcService);
    expect(service).toBeTruthy();
  });
});
