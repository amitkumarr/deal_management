import { Component, OnInit ,Input, OnChanges,SimpleChanges,SimpleChange} from '@angular/core';
import { DashboardSvcService } from '../dashboard-svc.service';
import {Item} from '../Item';

@Component({
  selector: 'app-cost-change-number',
  templateUrl: './cost-change-number.component.html',
  styleUrls: ['./cost-change-number.component.css']
})
export class CostChangeNumberComponent implements OnChanges, OnInit {
  public errorMessage = "";
  @Input() chartItems:Item[];
  @Input() itemCode:string;
  itemHistoryFound = false;
  costChangeCountList:any = [];
  selectedItem : string;


  // options for the chart
  view: any[] = [];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Year';
  showYAxisLabel = true;
  yAxisLabel = 'Count';
  timeline = true;
  legendTitle ="";
  groupPadding = 16;
  barPadding = 2;
  legendPosition='below';
  roundEdges=false;
  colorScheme = {
    domain: ['#009bef', '#95D13C', '#785EF0', '#F87EAC']
  };
  constructor(private DashBoardService: DashboardSvcService) {

  }


  getCostChangeCountList():void {
     this.DashBoardService.getCostChangeCountByItem(  this.selectedItem)
     .subscribe( res => {
            this.costChangeCountList = res;
            console.log("Cost Change History Count  : " + this.costChangeCountList.length);
            if(this.costChangeCountList.length>0){
              this.itemHistoryFound = true;
              this.errorMessage = "";
            }else{
              this.itemHistoryFound = false;
              this.errorMessage = "No History for Item No : "+this.selectedItem;
           }
         },
         error => {
                  this.errorMessage = error;
                  this.itemHistoryFound = false;
        }
     );

   }
  ngOnChanges(changes: SimpleChanges) {
     if(this.chartItems.length > 0){
       this.selectedItem = this.chartItems[0]['retailitemnumber'];
       this.getCostChangeCountList();
       console.log('chart data previous values : ', this.chartItems[0]);
     }

  }

  ngOnInit() {
     if(this.chartItems.length > 0){
      this.selectedItem = this.chartItems[0]['retailitemnumber'];
      this.getCostChangeCountList();
    }
  }

}
