import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostChangeNumberComponent } from './cost-change-number.component';

describe('CostChangeNumberComponent', () => {
  let component: CostChangeNumberComponent;
  let fixture: ComponentFixture<CostChangeNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostChangeNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostChangeNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
