export class Item {
  retailitemnumber: string;
  description: string;
  retailitemid:number;
  constructor() {
      this.retailitemnumber = "";
      this.description = "";
      this.retailitemid = 0;

   }
   resetItem(){
     this.retailitemnumber = "";
     this.description = "";
     this.retailitemid = 0;
    }
    convertItem(Item){
      this.retailitemnumber =   Item.retailitemnumber;
      this.description =   Item.description;
      this.retailitemid =   Item.retailitemid;

      return this;
    }

}
