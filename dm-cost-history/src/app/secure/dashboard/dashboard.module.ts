import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from './home/home.component';
import { DashboardModuleRouting } from './dashboard-routing';
import { SharedModule } from './../../shared/shared.module';
import { CostChangeNumberComponent } from './cost-change-number/cost-change-number.component';
import { CostChangeUnitComponent } from './cost-change-unit/cost-change-unit.component';
import { CostItemComponent } from './cost-item/cost-item.component';
import { CostItemSearchComponent } from './cost-item-search/cost-item-search.component';
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { DashboardSvcService } from './dashboard-svc.service';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardModuleRouting,
    NgxChartsModule,
    FormsModule,
   TypeaheadModule

  ],
  declarations: [HomeComponent, CostChangeNumberComponent, CostChangeUnitComponent, CostItemComponent, CostItemSearchComponent],
  providers:[DashboardSvcService]
})
export class DashboardModule { }
