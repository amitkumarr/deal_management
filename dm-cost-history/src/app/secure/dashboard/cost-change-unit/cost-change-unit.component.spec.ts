import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostChangeUnitComponent } from './cost-change-unit.component';

describe('CostChangeUnitComponent', () => {
  let component: CostChangeUnitComponent;
  let fixture: ComponentFixture<CostChangeUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostChangeUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostChangeUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
