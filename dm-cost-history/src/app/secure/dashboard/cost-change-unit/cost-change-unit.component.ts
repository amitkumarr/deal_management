import { Component, OnInit } from '@angular/core';
import { DashboardSvcService } from '../dashboard-svc.service';

@Component({
  selector: 'app-cost-change-unit',
  templateUrl: './cost-change-unit.component.html',
  styleUrls: ['./cost-change-unit.component.css']
})
export class CostChangeUnitComponent implements OnInit {

  public errorMessage = "";
  baseValue = 10000000;
  costUnitList:any = [];
  costListUnitInPercentage:any = [];
  chartData : any [];
  view: any[] = [];

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Month';
  showYAxisLabel = true;
  yAxisLabel = 'Unit Cost';
  timeline = true;
  legendTitle ='';
  legendPosition='below';
  isPercentageButton = false;
  colorScheme = {
    domain: ['#009bef', '#95D13C', '#785EF0', '#F87EAC']
  };
  private activeEntries : any[] = [{name:'Jan',value:7300000}];
  constructor(private DashBoardService: DashboardSvcService) { }

  getCostUnitList():void {
     this.DashBoardService.getCostChangeUnit()
     .subscribe( res => {
          this.costUnitList = res;
          this.costListUnitInPercentage = JSON.parse(JSON.stringify(res));
          this.setCostInPercentage(  this.costListUnitInPercentage);
          this.setChartData(this.costUnitList);
      //    console.log("Cost Unit List Data - " + this.costUnitList);
        //  console.log("costListUnitInPercentage - " + this.costListUnitInPercentage);
         },
         error => this.errorMessage = error
     );

  }

    ngOnInit() {
      this.getCostUnitList();
    }

    onSelect(event) {
      this.activeEntries = [];
      this.activeEntries.push(event);
      console.log(event);
    }


    showUnitCostPercentage(showPercentage:boolean){
      if(showPercentage){
        this.yAxisLabel="Unit Cost Percentage";
        this.isPercentageButton = true;
        this.setChartData(this.costListUnitInPercentage);
      }else{
        this.yAxisLabel="Unit Cost";
        this.isPercentageButton = false;
        this.setChartData(this.costUnitList);
      }
    }

    setCostInPercentage(costList){
        costList.map(cost=>{
           this.setPercentage(cost);
        });
        this.costListUnitInPercentage  = costList;
      //  console.log("costListUnitInPercentage " + this.costListUnitInPercentage);
    }

    setPercentage(cost){
       cost.series.map( item=>{
           item.value = (item.value*100)/this.baseValue;
        });
      return cost;
    }
    setChartData(data){
        this.chartData = data;
    }
}
