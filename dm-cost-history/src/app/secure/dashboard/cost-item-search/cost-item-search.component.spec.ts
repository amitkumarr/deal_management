import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostItemSearchComponent } from './cost-item-search.component';

describe('CostItemSearchComponent', () => {
  let component: CostItemSearchComponent;
  let fixture: ComponentFixture<CostItemSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostItemSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostItemSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
