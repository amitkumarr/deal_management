import { Component, OnInit, EventEmitter, Output ,Input,OnChanges,SimpleChanges,SimpleChange } from '@angular/core';
import { DashboardSvcService } from '../dashboard-svc.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map,catchError,mergeMap ,tap } from 'rxjs/operators';
import {Item} from '../Item';

@Component({
  selector: 'app-cost-item-search',
  templateUrl: './cost-item-search.component.html',
  styleUrls: ['./cost-item-search.component.css']
})
export class CostItemSearchComponent implements OnChanges, OnInit {
  public errorMessage = "";
  noSuggestion :any=   ["No suggestions found"];
  itemId: string ;
  itemFound = false;
  isValidItem = false;

  inactiveItemId = "BT0011"; // to filter if If Item is not active
  maxLimitToSearch = 3;
  isError = false;
//
  asyncSelected: string = "";
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean ;
  dataSource: Observable<any>;
//
  selectedItem = new Item;

 @Output() updateHome = new EventEmitter<any>();
 @Output() currentSearch = new EventEmitter<any>();

 dataState:any = {};
 @Input() searchItemList: Item[];
 @Input() firstItem: Item;
 @Input() showSearchControl:boolean;

  constructor(private DashBoardService: DashboardSvcService,private http: HttpClient) { }

  ngOnInit() {
    this.searchItems();
    console.log(this.selectedItem);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("Changes");
  }
// This will search and return the list of item numbers by item no given in search box
  searchItems(){
    this.dataSource = Observable.create((observer: any) => {
          this.DashBoardService.getItemsById(this.asyncSelected).subscribe((result: any) => {
              if(result.length>0){
                observer.next(result);
              }else{
                  this.itemFound = false;
                  observer.next(this.noSuggestion);
              }
              console.log("Search Result :",result );
          })
      });
  }


    // on search typing
    changeTypeaheadLoading(e: boolean): void {
      this.typeaheadLoading = e;
      this.itemFound = false;
      this.isValidItem = false;
      this.selectedItem.resetItem();
    }

    // Selected value event
    typeaheadOnSelect(e: TypeaheadMatch): void {
      console.log('Selected Item: ', e.value);
      if(this.validateItemOnSelect(e.value)){
        this.getItemDetail(this.asyncSelected);
        this.resetError();
      }

    }
    changeTypeaheadNoResult(e: boolean): void {
      this.typeaheadNoResults = e;
      if(!this.typeaheadNoResults){
        this.resetError();
      }else{
        this.itemFound = false;
        this.selectedItem.resetItem();
      }
    }

  ////

   addSearchItem() {

     console.log("this.isError",this.isError);
    if(this.validateItemToAdd()){
       this.updateSearchItemList();
       this.resetSearch();
       this.updateData();
     }else{
       this.setErrors("Please search and select item first");
     }
   }

   updateSearchItemList(){
     let item = Object.assign({},this.selectedItem);
     if( this.searchItemList.length < this.maxLimitToSearch){
       this.searchItemList.push(item);
     }
     if(this.searchItemList.length == this.maxLimitToSearch){
       this.showSearchControl= false;
     }else{
       this.showSearchControl= true;

     }

   }

     removeControl(){
       this.showSearchControl= false;
       this.resetSearch();
       this.updateData();
     }
     updateData(){
       this.dataState['showSearchControl'] = this.showSearchControl;
       this.dataState['searchItemList'] = this.searchItemList;
    //   this.dataState['searchedItem'] = this.selectedItem;

       this.updateHome.emit(this.dataState);
     }
     updateSearchListOnCompare(){
       this.addSearchItem();
       this.showSearchControl= false;
       this.updateData();
     }

// This will fetch the selected item detail
   getItemDetail(itemId){
       this.DashBoardService.getItem(itemId)
       .subscribe( (res :Item) => {
            if (res.hasOwnProperty('retailitemnumber') && !res.retailitemnumber.includes(this.inactiveItemId)) {
                this.itemFound = true;
                this.selectedItem =  res;
                this.isValidItem = true;
                this.resetError();
                this.currentSearch.emit( this.selectedItem);
            }else{
                this.setErrors("This item is not valid");
                this.isValidItem = false;
                this.selectedItem.resetItem();
            }
            console.log("Selected Item Detail : " + this.selectedItem);
            //
          },
           error =>     this.setErrors(error)
       );
   }

// To Check if item is already added to compare
   isItemExistsToCompare(itemId){
      let isItemExists = false;
      let list = [];
      list =  this.searchItemList.filter(item=>    item.retailitemnumber === itemId );
      if((list.length>0) ||(itemId === this.firstItem.retailitemnumber)){
        isItemExists = true;
      }
      return isItemExists;
   }

   // This will validate the selected search Item for duplicate or blank value
   validateItemOnSelect(selectedId){
       let isValid = false;
       if(selectedId === this.noSuggestion[0]){
         this.setErrors("Please search and select item first");
         isValid = false;
       }else if(this.isItemExistsToCompare(selectedId)){
         this.setErrors("This item is already added to compare");
         isValid = false;
       }else{
          isValid = true;
       }
       return isValid;
   }

// This will enable disable the '+' icon
   validateItemToAdd(){
       if((this.selectedItem.retailitemnumber === "")||(this.asyncSelected === "")){
         this.isValidItem = false;
       }else{
         this.isValidItem = true;
       }
       return    this.isValidItem;
   }

// To set the error message
   setErrors(msg){
       this.isError = true;
       this.errorMessage = msg;
   }
   // To clear the error message
   resetError(){
     this.isError = false;
     this.errorMessage = "";
   }

   resetSearch(){
     this.isValidItem = false;
     this.itemFound = false;
     this.asyncSelected="";
     this.selectedItem.resetItem();
     this.resetError();

   }
}
