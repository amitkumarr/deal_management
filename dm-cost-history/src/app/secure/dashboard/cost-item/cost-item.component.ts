import { Component, OnInit,EventEmitter ,Input,Output, OnChanges,SimpleChanges,SimpleChange} from '@angular/core';
import {Item} from '../Item';

@Component({
  selector: 'app-cost-item',
  templateUrl: './cost-item.component.html',
  styleUrls: ['./cost-item.component.css']
})
export class CostItemComponent implements  OnChanges, OnInit {
  @Input() searchItemList;
  @Input() showSearchControl;
  maxLimitToShow = 3;
  errorMessage = "";
  @Output() updateHome = new EventEmitter<any>();
  dataState:any = {};
  constructor() {

  }

  ngOnInit() {
    console.log(" Search Item List length : ",this.searchItemList);

  }
  ngOnChanges(changes: SimpleChanges) {
     console.log(' Search List : ', this.searchItemList);
  }

  showSearch(){
    if(this.searchItemList.length == this.maxLimitToShow){
      this.showSearchControl=false;
      this.errorMessage = "Not more than 4 item to compare";
    }else{
      this.showSearchControl=true;
      this.errorMessage = "";


    }
    this.updateData();
  }

  removeItem(removedItem){
    let index;
    let result = this.searchItemList.filter((item,i)=>{
        if(item.retailitemnumber === removedItem.retailitemnumber) {
          index = i;
        }
    });
    this.searchItemList.splice(index,1);
    this.errorMessage = "";
    this.updateData();
  }

  updateData(){
    this.dataState['showSearchControl'] = this.showSearchControl;
    this.dataState['searchItemList'] = this.searchItemList;
    this.dataState['errorMessage'] = this.errorMessage;

    this.updateHome.emit(this.dataState);
  }
}
