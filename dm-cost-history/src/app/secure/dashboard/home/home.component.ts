import { Component, OnInit ,EventEmitter, Output ,Input } from '@angular/core';
import { DashboardSvcService } from '../dashboard-svc.service';
import {Item} from '../Item';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements   OnInit {

  errorMessage = "";
  isError = false;
  itemList:Item[] = [];
  chartItemList:Item[] = [];
  searchItemList:Item[] = [];

  disableCompare = true;
  showSearchControl:boolean = false;
  selectedItem = new Item;
  currentSearchItem = new Item;

  maxLimitToCompare = 4;
  defaultRetailNumber:string = "RT0012";


  constructor(private DashBoardService: DashboardSvcService) {
    this.fillItemDropDownList();
  }

  ngOnInit() {

  }

  onItemChange(event:any){
    let previousItem = this.selectedItem;
    this.selectedItem = event;
    this.replaceItem(previousItem);
    console.log(  this.chartItemList);
  }



  replaceItem(currentItem):void{
      let index = -1;
      let result = this.chartItemList.filter((item,i)=>{
          if(item.retailitemnumber === currentItem.retailitemnumber) {
            index = i;
          }
      });
     if(index!== -1){
         this.chartItemList[index]= this.selectedItem ;
      }

  }

  // Tp fill the item list drop down
  fillItemDropDownList():void {
     this.DashBoardService.getItemList()
     .subscribe( res => {
           this.itemList = res.items;
           this.selectedItem= res.items.filter(item=>item.retailitemnumber==this.defaultRetailNumber)[0];
           this.chartItemList = [this.selectedItem];
         },
         error => this.errorMessage = error
     );
  }



//
  syncChartList(){
         this.chartItemList = [this.selectedItem];
         this.chartItemList =    this.mergeItemList(this.chartItemList,this.searchItemList);
         console.log("Final Chart Items List :",  this.chartItemList);
  }

  syncCurrentSearchItem(){
      let currentItem =  Object.assign({},this.currentSearchItem);
      if(currentItem.retailitemnumber!==""){
        this.searchItemList.push(currentItem );
        this.currentSearchItem.resetItem();
      }
  }

  compareChart(){
      this.syncCurrentSearchItem();
      this.syncChartList();
      if(this.chartItemList.length <2){
        this.setErrors("Minimum two items need to compare ");
      } else{
        this.resetError();
        this.showSearchControl = false;
      }
  }
  // To show first search control
  showSearchOption():void{
    this.disableCompare =  false ;
    this.showSearchControl = true;
  }

// to sync the item list
  mergeItemList(sourceList,targetList){
    let key = "retailitemnumber";
    let mergedList = sourceList.filter( sourceItem => ! targetList.find ( targetItem => sourceItem[key] === targetItem[key]) );
    return mergedList.concat(targetList);
  }

  getCurrentSearchItem($event){
    this.currentSearchItem = $event;
    this.resetError();
  }


  updateDashboard($event){
    let msg = $event['errorMessage'];
    this.showSearchControl = $event['showSearchControl'];
    this.searchItemList = $event['searchItemList'];
    if( msg === undefined){   msg = "";  }
    this.setErrors(msg);

    if((this.searchItemList.length>0)||(this.showSearchControl)){
      this.disableCompare = false;
    }else{
      this.disableCompare = true;
      this.syncChartList();
    }

  }


  // To set the error message
     setErrors(msg){
         if(msg!==""){
           this.isError = true;
         }else{
           this.isError = false;
         }
         this.errorMessage = msg;
     }
     // To clear the error message
     resetError(){
       this.isError = false;
       this.errorMessage = "";
     }


  ///
}
