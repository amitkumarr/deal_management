import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const BASE_URL =  environment.baseUrl;
const LIVE_URL_PATH = BASE_URL+'api/';
const LOCAL_URL_PATH = BASE_URL+'assets/local/';


export const IS_API = false;
//export const IS_API = true;


/*Http Options Section */
export const HTTP_OPTIONS = {
        headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
        })
};
/*Http Options Section */


/** Cost Item List URLs **/
const itemListLiveUrl = LIVE_URL_PATH + 'items';
const itemListLocalUrl = LOCAL_URL_PATH + 'items.json';
export const ITEM_LIST_URL = IS_API ? itemListLiveUrl : itemListLocalUrl;

/** Cost Item List URLs **/

/** Cost Change Count URLs **/
const costChangeCountListLiveUrl = LIVE_URL_PATH + 'items';
const costChangeCountListLocalUrl = LOCAL_URL_PATH + 'cost-change-count.json';
export const COST_COUNT_LIST_URL = IS_API ? costChangeCountListLiveUrl : costChangeCountListLocalUrl;

/** Cost Change Unit URLs **/
/** Cost Item List URLs **/
const costUnitListLiveUrl = LIVE_URL_PATH + 'items';
const costUnitListLocalUrl = LOCAL_URL_PATH + 'cost-change-unit.json';
export const COST_UNIT_LIST_URL = IS_API ? costUnitListLiveUrl : costUnitListLocalUrl;

/** Cost Item List URLs **/
