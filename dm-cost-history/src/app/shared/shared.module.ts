import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule,TranslateService} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [],
  exports:[TranslateModule]
})
export class SharedModule {
  constructor(private translate: TranslateService) {
  			let currentLanguage :any;
  			let browserLang = translate.getBrowserLang();
  			translate.addLangs(["en", "fr"]);
  			translate.setDefaultLang('en');
  			translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');

        console.log("Language ",browserLang);
  		}
}
