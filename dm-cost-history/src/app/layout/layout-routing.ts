import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { SecureLayoutComponent } from './secure-layout/secure-layout.component';
import { SecureNavigationMenuComponent } from './secure-navigation-menu/secure-navigation-menu.component';
import { DashboardModule } from '../secure/dashboard/dashboard.module';
import { NonSecureModule } from '../public/non-secure/non-secure.module';



 const routes: Routes = [
   {   path: '',
     component: SecureLayoutComponent,
     children: [
       { path: '',  redirectTo: '/dashboard',  pathMatch: 'full'  },
       { path: 'dashboard', loadChildren: ()=>DashboardModule }
     ]
  },

  {   path: '',
     component: PublicLayoutComponent,
     children: [
      { path: '',  redirectTo: '/login',  pathMatch: 'full'  },
      { path: 'login', loadChildren: ()=>NonSecureModule },
      { path: '**', loadChildren: ()=>NonSecureModule }

     ]
  },



     { path: '**', component: PublicLayoutComponent }


];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class LayoutRouting { }
