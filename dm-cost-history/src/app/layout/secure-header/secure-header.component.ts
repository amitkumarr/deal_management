import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-secure-header',
  templateUrl: './secure-header.component.html',
  styleUrls: ['./secure-header.component.css']
})
export class SecureHeaderComponent implements OnInit {
  title = 'IBM Merchandising';
  titleClass = 'headerContainer';
  constructor() { }

  ngOnInit() {
  }
}
