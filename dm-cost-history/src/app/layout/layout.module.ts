import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { SecureLayoutComponent } from './secure-layout/secure-layout.component';
import { SecureHeaderComponent } from './secure-header/secure-header.component';
import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { PublicHeaderComponent } from './public-header/public-header.component';
import { SecureNavigationMenuComponent } from './secure-navigation-menu/secure-navigation-menu.component';
//import { DashboardModule } from '../secure/dashboard/dashboard.module';
import { LayoutRouting } from './layout-routing';
//import { NonSecureModule } from '../public/non-secure/non-secure.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutRouting,
    TranslateModule,
    FormsModule

  ],
  declarations: [SecureLayoutComponent, SecureHeaderComponent, PublicLayoutComponent, PublicHeaderComponent, SecureNavigationMenuComponent],
  exports: []

})
export class LayoutModule { }
