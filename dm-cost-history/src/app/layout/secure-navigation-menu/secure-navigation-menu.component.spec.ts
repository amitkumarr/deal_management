import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureNavigationMenuComponent } from './secure-navigation-menu.component';

describe('SecureNavigationMenuComponent', () => {
  let component: SecureNavigationMenuComponent;
  let fixture: ComponentFixture<SecureNavigationMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureNavigationMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureNavigationMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
