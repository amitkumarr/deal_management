import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { IS_API, HTTP_OPTIONS, VENDOR_DATA_URL, VENDOR_DATA_MONTHLY_URL } from './../../common/config';
@Injectable(
  //  {  providedIn: 'root'}
)
export class VendorDataService {

  constructor(private httpClient: HttpClient) { }
  errorHandler(error: HttpErrorResponse) {
    console.log(JSON.stringify(error));
    return throwError("Server error ...");
  }

  getVendorsList(): Observable<any> {
    console.log("USER_DATA_URL : ", VENDOR_DATA_URL);
    let errorHandler = this.errorHandler.bind(this);

    if (IS_API) {
      return this.httpClient.get<any[]>(VENDOR_DATA_URL, HTTP_OPTIONS)
        .pipe(catchError(errorHandler));
    } else {
      return this.httpClient.get<any[]>(VENDOR_DATA_URL).pipe(
        catchError(errorHandler)
      );
    }
  }

  getVendorsListByMonth(): Observable<any> {
    console.log("VENDOR_DATA_MONTHLY_URL : ", VENDOR_DATA_MONTHLY_URL);
    let errorHandler = this.errorHandler.bind(this);

    if (IS_API) {
      return this.httpClient.get<any[]>(VENDOR_DATA_MONTHLY_URL , HTTP_OPTIONS)
        .pipe(  catchError(errorHandler) );
    } else {
      return this.httpClient.get<any[]>(VENDOR_DATA_MONTHLY_URL).pipe(
        catchError(errorHandler)
      );
    }
  }

}
