import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorByNumberComponent } from './vendor-by-number.component';

describe('VendorByNumberComponent', () => {
  let component: VendorByNumberComponent;
  let fixture: ComponentFixture<VendorByNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorByNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorByNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
