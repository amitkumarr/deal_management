import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartErrorEvent, ChartEvent, GoogleChartComponent } from 'angular-google-charts';
import { Router } from '@angular/router';
import { EmptyObject } from './../../../common/EmptyObjectType';
import { ChartOptions } from './../../../common/ChartConfigType';
import { VendorDataService } from '../vendor-data.service';
import { UtilService } from './../../../common/util.service';

@Component({
  selector: 'app-vendor-by-number',
  templateUrl: './vendor-by-number.component.html',
  styleUrls: ['./vendor-by-number.component.css']
})
export class VendorByNumberComponent implements OnInit {
  chart: EmptyObject = {};
  element: ElementRef;
  vendorNumber = 0;
  loading = true;
  isError = false;
  public errorMessage = "";

  // @ViewChild('chart') chart: GoogleChartComponent;
  constructor(private vendorService: VendorDataService,private util: UtilService) {

    this.chart.data = [];
    this.chart.title = "";
    this.chart.type = "PieChart";
    this.chart.roles = [];
    this.chart.options = { ...this.chart.options, ...ChartOptions };
    this.getChartData();
    console.log(JSON.stringify(this.chart));
  }

  ngOnInit() {
  }
  onMouseEnter(e) {

  }
  onMouseLeave(e) {

  }
  onReady() {

  }
  onSelect(e) {

  }
  onError(e) {

  }

  getChartData(): void {
    this.vendorService.getVendorsList().subscribe(res => {
      this.vendorNumber = res.VendorData.count;
      this.makeChartData(res.VendorData.Vendors);
      console.log(res);
    },
    error => {
    // this.errorMessage = error;
      this.isError= true;this.loading = false;
      this.errorMessage =  this.util.getTranslatedText('APP.MSG.ERROR_DATA_NOT_FOUND');

    },
    () => {
      this.loading = false;
      this.isError = false;
    });
  }

  makeChartData(result) {
    let finalArray = result.map(function (obj) {
      let temp = [];
      temp.push(obj.type);
      temp.push(obj.number);
      return temp;
    });
    this.chart.data = finalArray;
    console.log(finalArray);
  }
}
