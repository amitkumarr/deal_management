import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HomeComponent } from './home/home.component';
import { DashboardModuleRouting } from './dashboard-routing';
import { UserByNumberComponent } from './user-by-number/user-by-number.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { VendorByNumberComponent } from './vendor-by-number/vendor-by-number.component';
import { VendorMonthlyComponent } from './vendor-monthly/vendor-monthly.component';
import { UserMonthlyComponent } from './user-monthly/user-monthly.component';
import { UserDataService } from './user-data.service';
import { VendorDataService } from './vendor-data.service';
import { SharedComponentModule} from './../../shared-component/shared-component.module';

import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    DashboardModuleRouting,
    GoogleChartsModule,
    TranslateModule,
    SharedComponentModule
  ],
  declarations: [HomeComponent, UserByNumberComponent, VendorByNumberComponent, VendorMonthlyComponent, UserMonthlyComponent],
  providers:[UserDataService,VendorDataService]

})
export class DashboardModule { }
