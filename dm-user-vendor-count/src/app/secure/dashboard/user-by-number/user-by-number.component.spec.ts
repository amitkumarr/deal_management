import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserByNumberComponent } from './user-by-number.component';

describe('UserByNumberComponent', () => {
  let component: UserByNumberComponent;
  let fixture: ComponentFixture<UserByNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserByNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserByNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
