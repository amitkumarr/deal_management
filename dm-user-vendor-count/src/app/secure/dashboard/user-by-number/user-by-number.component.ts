import { Component, OnInit, ViewChild, ElementRef, OnChanges, AfterViewInit } from '@angular/core';
//import { ChartErrorEvent, ChartEvent, GoogleChartComponent } from 'projects/angular-google-charts/src/public_api';
import { ChartErrorEvent, ChartEvent, GoogleChartComponent } from 'angular-google-charts';
import { Router } from '@angular/router';
import { UserDataService } from '../user-data.service';
import { EmptyObject } from './../../../common/EmptyObjectType';
import { ChartOptions } from './../../../common/ChartConfigType';
import { debounceTime } from 'rxjs/operators';
import { Observable, fromEvent } from 'rxjs';
import { UtilService } from './../../../common/util.service';


@Component({
  selector: 'app-user-by-number',
  templateUrl: './user-by-number.component.html',
  styleUrls: ['./user-by-number.component.css']
})
export class UserByNumberComponent implements OnInit, OnChanges, AfterViewInit {

  chart: EmptyObject = {};

  // @ViewChild('chart1')  chart1: GoogleChartComponent;
  element: ElementRef;
  userCount: number=0;
  loading = true;
  isError = false;
  public errorMessage = "";
  constructor(private userService: UserDataService,private util: UtilService) {
    this.chart.data = [];
    this.chart.title = "";
    this.chart.type = "PieChart";
    this.chart.columnNames = ['User', 'Count'];
    this.chart.roles = [];
    this.chart.options = { ...this.chart.options, ...ChartOptions };
    this.getChartData();
    console.log(JSON.stringify(this.chart.options));
  }

  ngOnInit() {


  }
  onMouseEnter(e) {

  }
  onMouseLeave(e) {

  }
  onReady() {

  }
  onSelect(e) {

  }
  onError(e) {

  }
  ngAfterViewInit() {

  }
  ngOnChanges() {

  }
  getChartData(): void {
    this.userService.getUsersList().subscribe(res => {
      this.userCount = res.UserData.count;
      this.makeChartData(res.UserData.Users);
      console.log(res);
    },
      error => {
        this.errorMessage =  this.util.getTranslatedText('APP.MSG.ERROR_DATA_NOT_FOUND');
        this.isError= true;this.loading = false;
      },
      () => {
        this.loading = false;
        this.isError = false;
      });
  }

  makeChartData(result) {

    let finalArray = result.map(function (obj) {
      let temp = [];
      temp.push(obj.type);
      temp.push(obj.number);
      return temp;
    });
    this.chart.data = finalArray;
    console.log("Data ", finalArray);
  }


}
