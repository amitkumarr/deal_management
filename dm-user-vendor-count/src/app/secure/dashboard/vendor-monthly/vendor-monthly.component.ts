import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartErrorEvent, ChartEvent, GoogleChartComponent } from 'angular-google-charts';
import { Router } from '@angular/router';
import { VendorDataService } from '../vendor-data.service';
import { EmptyObject } from './../../../common/EmptyObjectType';
import { ChartOptions } from './../../../common/ChartConfigType';
import { UtilService } from './../../../common/util.service';

@Component({
  selector: 'app-vendor-monthly',
  templateUrl: './vendor-monthly.component.html',
  styleUrls: ['./vendor-monthly.component.css']
})
export class VendorMonthlyComponent implements OnInit {
  charts: Array<{
    title: string,
    type: string,
    data: Array<Array<string | number | {}>>,
    roles: Array<{ type: string, role: string, index?: number }>,
    columnNames?: Array<string>,
    options?: {}
  }> = [];
  chart: EmptyObject = {};
  loading = true;
  public errorMessage = "";
    isError = false;
  //@ViewChild('chart')    chart: GoogleChartComponent;
  constructor(private vendorService: VendorDataService,private util: UtilService) {
    this.chart.data = [];
    this.chart.title = "";
    this.chart.type = "LineChart";
    this.chart.columnNames = ['Month'];
    this.chart.roles = [];
  /*  this.chart.data = [
      ['Jan', 2000, 8000],
      ['Feb', 2000, 8000],
      ['March', 2000, 8000],
      ['April', 2000, 8000],
      ['May', 2000, 8000],
      ['June', 2000, 8000],
      ['July', 2000, 8000],
      ['Aug', 2000, 8000],
      ['Sep', 2000, 8000],
      ['Oct', 2000, 8000],
      ['Nov', 2000, 8000],
      ['Dec', 2000, 8000],
    ];*/
    this.chart.options = { ...this.chart.options, ...ChartOptions };
    this.getChartData();

  }
  getChartData(): void {
    this.vendorService.getVendorsListByMonth().subscribe(res => {
      this.getCoulmnData(res.VendorData_Monthly);
      this.getMonths(res.VendorData_Monthly);
      this.processChartData(res.VendorData_Monthly);
      console.log(res);

    },
      error => {
        this.errorMessage =  this.util.getTranslatedText('APP.MSG.ERROR_DATA_NOT_FOUND');
        this.isError= true;this.loading = false;
            },
      () => {
        this.loading = false;
          this.isError= false;
      });
  }

  getCoulmnData(result){
     let columns = result.map(object=>{
      return object.type;
    });

   this.chart.columnNames = this.chart.columnNames.concat(columns);
   console.log("Chart Columns ",this.chart.columnNames);
  }

  getMonths(result){
    let data = result.map(vendor =>     vendor.data.map(item=>item.month)  );
    this.chart.months = data[0];
    console.log("Chart Months ",this.chart.months);
  }

  getMonthVendors(vendorMonthData,month){
     let monthlyVendorList = [month];
     let temp = vendorMonthData.map(monthDataList=>{
        let monthData = monthDataList.filter(monthItem=>
           monthItem.month === month
         )
         monthlyVendorList.push(monthData[0].count);
         return monthlyVendorList;
    }

    );
  //  console.log("monthlyVendorList ",monthlyVendorList);
    return monthlyVendorList;
  }
  setChartData(monthsData,months){
      let chartData = [];
      let temp = [];
      temp = months.map(month=>{
            temp = this.getMonthVendors(monthsData,month);
            chartData.push(temp);
          });
      console.log("Final Chart Data ",chartData);
      this.chart.data = chartData;
  }
  processChartData(result) {

    let months = this.chart.months;
     let monthsData = result.map(vendor=>{
          let temp = vendor.data.filter(item=>  months.indexOf(item.month)!==-1);
          return temp;
     });
     this.setChartData(monthsData,months);
  }
  ngOnInit() {
  }
  onMouseEnter(e) {

  }
  onMouseLeave(e) {

  }
  onReady() {

  }
  onSelect(e) {

  }
  onError(e) {

  }

}
