import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMonthlyComponent } from './vendor-monthly.component';

describe('VendorMonthlyComponent', () => {
  let component: VendorMonthlyComponent;
  let fixture: ComponentFixture<VendorMonthlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMonthlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
