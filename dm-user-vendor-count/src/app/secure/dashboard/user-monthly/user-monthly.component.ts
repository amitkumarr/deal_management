import { Component, OnInit,ViewChild } from '@angular/core';
import {ChartErrorEvent, ChartEvent, GoogleChartComponent} from 'angular-google-charts';
import { Router } from '@angular/router';
import { UserDataService } from '../user-data.service';
import { EmptyObject } from './../../../common/EmptyObjectType';
import { ChartOptions } from './../../../common/ChartConfigType';
import { UtilService } from './../../../common/util.service';

@Component({
  selector: 'app-user-monthly',
  templateUrl: './user-monthly.component.html',
  styleUrls: ['./user-monthly.component.css']
})
export class UserMonthlyComponent implements OnInit {
  charts: Array<{
      title: string,
      type: string,
      data: Array<Array<string | number | {}>>,
      roles: Array<{ type: string, role: string, index?: number }>,
      columnNames?: Array<string>,
      options?: {}
    }> = [];
    chart: EmptyObject = {};
    loading = true;
    public errorMessage = "";
    isError = false;


  //  @ViewChild('chart')    chart: GoogleChartComponent;
  constructor(private userService: UserDataService,private util: UtilService) {

  /*  this.chart.data =  [
      ['Jan', 200, 300,400, 500,700, 1000, 2000],
      ['Feb', 200, 300,400, 500,700, 1000, 2000],
      ['March', 200, 300,400, 500,700, 1000, 2000],
      ['April', 200, 300,400, 500,700, 1000, 2000],
      ['May', 200, 300,400, 500,700, 1000, 2000],
      ['June', 200, 300,400, 500,700, 1000, 2000],
      ['July', 200, 300,400, 500,700, 1000, 2000],
      ['Aug', 200, 300,400, 500,700, 1000, 2000],
      ['Sep', 200, 300,400, 500,700, 1000, 2000],
      ['Oct', 200, 300,400, 500,700, 1000, 2000],
      ['Nov', 200, 300,400, 500,700, 1000, 2000],
      ['Dec', 200, 300,400, 500,700, 1000, 2000],
    ];*/
      this.getChartData();

   }

   getChartData():void{
     this.userService.getUsersListByMonth().subscribe(res => {
       this.getCoulmnData(res.UserData_Monthly);
       this.getMonths(res.UserData_Monthly);
       this.processChartData(res.UserData_Monthly);
       console.log(res);

     },
     error => {
       this.errorMessage =  this.util.getTranslatedText('APP.MSG.ERROR_DATA_NOT_FOUND');
       this.isError= true;this.loading = false;
          },
     () => {
       this.loading = false;
        this.isError= false;
     });
   }

   getCoulmnData(result){
      let columns = result.map(object=>{
       return object.type;
     });

    this.chart.columnNames = this.chart.columnNames.concat(columns);
    console.log("Chart Columns ",this.chart.columnNames);
   }

   getMonths(result){
     let data = result.map(user =>     user.data.map(item=>item.month)  );
     this.chart.months = data[0];
     console.log("Chart Months ",this.chart.months);
   }

   getMonthUsers(userMonthData,month){
      let monthlyUserList = [month];
      let temp = userMonthData.map(monthDataList=>{
         let monthData = monthDataList.filter(monthItem=>
            monthItem.month === month
          )
          monthlyUserList.push(monthData[0].count);
          return monthlyUserList;
     }

     );
  //   console.log("monthlyUserList ",monthlyUserList);
     return monthlyUserList;
   }
   setChartData(monthsData,months){
       let chartData = [];
       let temp = [];
       temp = months.map(month=>{
             temp = this.getMonthUsers(monthsData,month);
             chartData.push(temp);
           });
       console.log("Final Chart Data ",chartData);
       this.chart.data = chartData;
   }
   processChartData(result){

      let months = this.chart.months;
       let monthsData = result.map(user=>{
            let temp = user.data.filter(item=>  months.indexOf(item.month)!==-1);
            return temp;
       });
       this.setChartData(monthsData,months);

   }


  ngOnInit() {
    this.chart.data = [];
    this.chart.title ="";
    this.chart.type = "LineChart";
    this.chart.columnNames =['Month'] ;
    this.chart.roles = [];
    this.chart.options = {...this.chart.options,...ChartOptions};

  }
  onMouseEnter(e) {

 }
 onMouseLeave(e) {

 }
 onReady() {
   console.log("Chart is ready now");
 }
 onSelect(e) {

 }
 onError(e) {
   console.log("error",e);
 }

}
