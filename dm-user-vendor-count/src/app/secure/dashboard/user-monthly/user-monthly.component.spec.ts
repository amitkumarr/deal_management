import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMonthlyComponent } from './user-monthly.component';

describe('UserMonthlyComponent', () => {
  let component: UserMonthlyComponent;
  let fixture: ComponentFixture<UserMonthlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMonthlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
