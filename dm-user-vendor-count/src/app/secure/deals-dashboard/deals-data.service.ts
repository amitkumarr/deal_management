import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';

import {  IS_API, HTTP_OPTIONS,DEALS_DATA_MONTHLY_URL } from './../../common/config';
@Injectable(
//  {  providedIn: 'root'}
)
export class DealsDataService {

  constructor(private httpClient: HttpClient) { }
  errorHandler(error: HttpErrorResponse) {
      console.log(JSON.stringify(error));
      return throwError("Server error ...");
  }
  /** Api hit for item lists **/
  getDealsList(): Observable<any> {
    console.log("DEALS_DATA_MONTHLY_URL : ", DEALS_DATA_MONTHLY_URL);
    let errorHandler = this.errorHandler.bind(this);

    if (IS_API) {
      return this.httpClient.get<any[]>(DEALS_DATA_MONTHLY_URL , HTTP_OPTIONS)
        .pipe(  catchError(errorHandler) );
    } else {
      return this.httpClient.get<any[]>(DEALS_DATA_MONTHLY_URL).pipe(
        catchError(errorHandler)
      );
    }
  }
}
