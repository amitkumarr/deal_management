import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HomeComponent } from './home/home.component';
import { DealsMonthlyComponent } from './deals-monthly/deals-monthly.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { DealsDashboardModuleRouting } from './deals-dashboard-routing';
import { DealsDataService } from './deals-data.service';
import { SharedComponentModule} from './../../shared-component/shared-component.module';

@NgModule({
  imports: [
    CommonModule,
    DealsDashboardModuleRouting,
    GoogleChartsModule,
    TranslateModule,
    SharedComponentModule
  ],
  declarations: [HomeComponent, DealsMonthlyComponent],
  providers: [DealsDataService]
})
export class DealsDashboardModule { }
