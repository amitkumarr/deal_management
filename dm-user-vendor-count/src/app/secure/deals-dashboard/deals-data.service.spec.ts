import { TestBed } from '@angular/core/testing';

import { DealsDataService } from './deals-data.service';

describe('DealsDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealsDataService = TestBed.get(DealsDataService);
    expect(service).toBeTruthy();
  });
});
