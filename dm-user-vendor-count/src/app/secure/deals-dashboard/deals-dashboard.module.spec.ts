import { DealsDashboardModule } from './deals-dashboard.module';

describe('DealsDashboardModule', () => {
  let dealsDashboardModule: DealsDashboardModule;

  beforeEach(() => {
    dealsDashboardModule = new DealsDashboardModule();
  });

  it('should create an instance', () => {
    expect(dealsDashboardModule).toBeTruthy();
  });
});
