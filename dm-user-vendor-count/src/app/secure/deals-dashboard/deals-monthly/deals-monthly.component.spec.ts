import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealsMonthlyComponent } from './deals-monthly.component';

describe('DealsMonthlyComponent', () => {
  let component: DealsMonthlyComponent;
  let fixture: ComponentFixture<DealsMonthlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealsMonthlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealsMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
