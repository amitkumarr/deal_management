import { Component, OnInit,ViewChild } from '@angular/core';
import {ChartErrorEvent, ChartEvent, GoogleChartComponent} from 'angular-google-charts';
import { Router } from '@angular/router';
import { DealsDataService } from '../deals-data.service';
import { EmptyObject } from './../../../common/EmptyObjectType';
import { ChartOptions } from './../../../common/ChartConfigType';
import { UtilService } from './../../../common/util.service';

@Component({
  selector: 'app-deals-monthly',
  templateUrl: './deals-monthly.component.html',
  styleUrls: ['./deals-monthly.component.css']
})
export class DealsMonthlyComponent implements OnInit {
  charts: Array<{
    title: string,
    type: string,
    data: Array<Array<string | number | {}>>,
    roles: Array<{ type: string, role: string, index?: number }>,
    columnNames?: Array<string>,
    options?: {}
  }> = [];
  chart: EmptyObject = {};

  loading = true;
  isError = false;
  public errorMessage = "";

  //@ViewChild('chart')  chart: GoogleChartComponent;

  constructor(private dealService: DealsDataService,private util: UtilService) {
        this.chart.data=[];
        this.chart.title="";
        this.chart.type= "ColumnChart";

        this.chart.columnNames= ['Month'];//, 'Process','Accept','Modify','Submit','Resubmit','Rescind','Reject','Rescind Confirmed','Other'];
      /*  this.chart.data= [
          ['Apr', 100, 0,0,0,0,0,0,0,0],
          ['May', 100, 0,0,0,0,0,0,0,0],
          ['Jun', 100, 100,0,0,0,0,0,0,0],
          ['Jul', 100, 0,0,0,0,0,0,0,0],


          ['Sep', 100, 0,0,0,0,0,0,0,0],
          ['Oct', 0, 0, 0, 0, 0, 0, 0,0,0],
          ['Nov', 0, 0, 0, 0, 0, 0, 0,0,0],
          ['Dec', 100, 0,0,0,0,0,0,0,0],
          ['Jan', 100, 0,0,0,0,0,0,0,0],
          ['Feb', 0, 0, 0, 0, 0, 0, 0,0,0],
          ['Mar', 0, 0, 0, 0, 0, 0, 0,0,0],

        ],*/
        this.chart.roles=[];
        this.chart.options={
          bar: { groupWidth: '75%' },
          isStacked: true
        }
        this.chart.options = {...this.chart.options,...ChartOptions};
        this.getChartData();

  }
  getChartData():void{
    this.dealService.getDealsList().subscribe(res => {
      this.getCoulmnData(res.DealStatusData);
      this.getMonths(res.DealStatusData);
      this.makeChartData(res.DealStatusData);
      console.log(res);

    },
    error => {
      this.errorMessage =  this.util.getTranslatedText('APP.MSG.ERROR_DATA_NOT_FOUND');
      this.isError= true;this.loading = false;
        },
    () => {
      this.loading = false;
      this.isError = false;
    });
  }



  getCoulmnData(result){
     let columns = result.map(object=>{
      return object.type;
    });

   this.chart.columnNames = this.chart.columnNames.concat(columns);
   console.log("Chart Columns ",this.chart.columnNames);
  }

  getMonths(result){
    let data = result.map(deals =>     deals.data.map(item=>item.month)  );
    this.chart.months = data[0];
    console.log("Chart Months ",this.chart.months);
  }

  getMonthDeals(dealMonthData,month){
     let monthlyDealList = [month];
     let temp = dealMonthData.map(monthDataList=>{
        let monthData = monthDataList.filter(monthItem=>
           monthItem.month === month
         )
         monthlyDealList.push(monthData[0].count);
         return monthlyDealList;
    }

    );
    console.log("monthlyDealList ",monthlyDealList);
    return monthlyDealList;
  }
  setChartData(monthsData,months){
      let chartData = [];
      let temp = [];
      temp = months.map(month=>{
            temp = this.getMonthDeals(monthsData,month);
            chartData.push(temp);
          });
      console.log("Final Chart Data ",chartData);
      this.chart.data = chartData;
  }
  makeChartData(result){

    let months = this.chart.months;
     let monthsData = result.map(deals=>{
          let temp = deals.data.filter(item=>  months.indexOf(item.month)!==-1);
          return temp;
     });
     this.setChartData(monthsData,months);
  }
  ngOnInit() {
      this.getDealsData();
  }

    getDealsData():void{
      this.dealService.getDealsList().subscribe(res => {
        console.log(res);
      });
    }
    onMouseEnter(e) {

   }
   onMouseLeave(e) {

   }
   onReady() {

   }
   onSelect(e) {

   }
   onError(e) {

   }

}
