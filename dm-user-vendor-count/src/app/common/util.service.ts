import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EmptyObject } from './EmptyObjectType';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable(
  //{
  //providedIn: 'root'
//}
)
export class UtilService {

  constructor( private translateSvc: TranslateService) { }

  getTranslatedText(...args) {
      let key = args[0];
      let param = args[1];
      return this.translateSvc.instant(key, param);

  }
}
