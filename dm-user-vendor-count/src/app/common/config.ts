import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const BASE_URL =  environment.baseUrl;
const LIVE_URL_PATH = BASE_URL+'api/';
const LOCAL_URL_PATH = BASE_URL+'assets/local/';


export const IS_API = false;
//export const IS_API = true;


/*Http Options Section */
export const HTTP_OPTIONS = {
        headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
        })
};
/*Http Options Section */


/** User Count URLs **/
const userDataLiveUrl = LIVE_URL_PATH + 'usercount';
const userDataLocalUrl = LOCAL_URL_PATH + 'user-data.json';
export const USER_DATA_URL = IS_API ? userDataLiveUrl : userDataLocalUrl;


/** User Count Mothly URLs **/
const userDataMonthlyLiveUrl = LIVE_URL_PATH + 'usercountmonth';
const userDataMonthlyLocalUrl = LOCAL_URL_PATH + 'user-data-monthly.json';
export const USER_DATA_MONTHLY_URL = IS_API ? userDataMonthlyLiveUrl : userDataMonthlyLocalUrl;


/** Vendor Count URLs **/
const vendorDataLiveUrl = LIVE_URL_PATH + 'vendorcount';
const vendorDataLocalUrl = LOCAL_URL_PATH + 'vendor-data.json';
export const VENDOR_DATA_URL = IS_API ? vendorDataLiveUrl : vendorDataLocalUrl;


/** Vendor Count Mothly URLs **/
const vendorDataMonthlyLiveUrl = LIVE_URL_PATH + 'vendorcountmonth';
const vendorDataMonthlyLocalUrl = LOCAL_URL_PATH + 'vendor-data-monthly.json';
export const VENDOR_DATA_MONTHLY_URL = IS_API ? vendorDataMonthlyLiveUrl : vendorDataMonthlyLocalUrl;

/** Deal Mothly URLs **/
const dealsDataMonthlyLiveUrl = LIVE_URL_PATH + 'dealscountmonth';
const dealsDataMonthlyLocalUrl = LOCAL_URL_PATH + 'deals-data-monthly.json';
export const DEALS_DATA_MONTHLY_URL = IS_API ? dealsDataMonthlyLiveUrl : dealsDataMonthlyLocalUrl;
