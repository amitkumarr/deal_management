export const ChartOptions = {
    colors:['#009bef', '#95D13C', '#785EF0', '#F87EAC',
            '#FFB000','#00B6CB','#FF5C49','#047CC0',
            '#00A78F','#FE8500','#40D5BB','#FF509E',
            '#3C6DF0','#81B532','#DC267F','#CB71D7',
            '#FE6100','#648FFF','#34BC6E','#9753E1'

          ],
      legend: {
        position: 'bottom',
        alignment: 'end',
        floating: true,
        textStyle : {
            fontSize: 12 // or the number you want
        }
      },
      series: {
        0: { pointShape: 'star'},
        1: { pointShape: 'square'},
        2: { pointShape: 'square' },
        3: { pointShape: 'diamond' },
        4: { pointShape: 'star' },
        5: { pointShape: 'polygon' }
    },
    hAxis : {
      textStyle : {
          fontSize: 9 // or the number you want
      }
    },
    height:400,
    pointSize: 7
};
