import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import {TranslateModule} from '@ngx-translate/core';

import { SecureLayoutComponent } from './secure-layout/secure-layout.component';
import { SecureHeaderComponent } from './secure-header/secure-header.component';
import { SecureNavigationMenuComponent } from './secure-navigation-menu/secure-navigation-menu.component';
//import { DashboardModule } from '../secure/dashboard/dashboard.module';
import { LayoutRouting } from './layout-routing';
//import { DealsDashboardModule } from '../secure/deals-dashboard/deals-dashboard.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutRouting,
    TranslateModule,
    FormsModule

  ],

  declarations: [SecureLayoutComponent, SecureHeaderComponent, SecureNavigationMenuComponent]
})
export class LayoutModule { }
