import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SecureLayoutComponent } from './secure-layout/secure-layout.component';
import { SecureNavigationMenuComponent } from './secure-navigation-menu/secure-navigation-menu.component';
import { DashboardModule } from '../secure/dashboard/dashboard.module';
//import { NonSecureModule } from '../public/non-secure/non-secure.module';
import { DealsDashboardModule } from '../secure/deals-dashboard/deals-dashboard.module';



 const routes: Routes = [
   {   path: '',
     component: SecureLayoutComponent,
     children: [
       { path: '',  redirectTo: '/user-dashboard',  pathMatch: 'full'  },
       { path: 'user-dashboard', loadChildren: ()=>DashboardModule },
       { path: 'deals-dashboard', loadChildren: ()=>DealsDashboardModule }

     ]
  }



    // { path: '**', component: SecureLayoutComponent }


];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class LayoutRouting { }
