import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { GoogleChartsModule } from 'angular-google-charts';
import { UtilService } from './common/util.service';

import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    GoogleChartsModule,
    RouterModule.forRoot([], { useHash: true, enableTracing: false }),
    TranslateModule.forRoot(
      {
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
    LayoutModule

  ],
  providers: [UtilService],
  bootstrap: [AppComponent]
})
export class AppModule {
  userLanguege = "en";
  constructor(private translate: TranslateService) {
    let currentLanguage: any;
    translate.addLangs(["en", "fr"]);
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang().match(/en|fr/) ? translate.getBrowserLang() : 'en';
    // currentLanguage = this.getCurrentLanguage();
    currentLanguage = this.userLanguege;
    if (currentLanguage !== null && currentLanguage !== '') {
      this.setCurrentLanguage(currentLanguage);
    } else {
      currentLanguage = browserLang;
      this.setCurrentLanguage(browserLang);

    }
    translate.use(currentLanguage);
    // translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  getCurrentLanguage() {
    let curLang = localStorage.getItem("language");
    console.log("curLang: ", curLang);
    return curLang;
  }

  setCurrentLanguage(lang: string) {
    localStorage.setItem("language", lang);
  }
}
