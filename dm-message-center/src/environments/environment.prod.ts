export const environment = {
  production: true,
  consoleLogging : false,
  baseUrl : location.protocol + "//" + location.host + "/"
};
