import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { MessageBoxModuleRouting } from './messagebox-routing';
import { SharedModule } from './../../shared/shared.module';
//import { CostChangeNumberComponent } from './cost-change-number/cost-change-number.component';
//import { CostChangeUnitComponent } from './cost-change-unit/cost-change-unit.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MessageBoxModuleRouting
  ],
  declarations: [HomeComponent]
})
export class MessageBoxModule { }
