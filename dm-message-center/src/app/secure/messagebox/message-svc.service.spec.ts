import { TestBed } from '@angular/core/testing';

import { MessageSvcService } from './message-svc.service';

describe('MessageSvcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageSvcService = TestBed.get(MessageSvcService);
    expect(service).toBeTruthy();
  });
});
