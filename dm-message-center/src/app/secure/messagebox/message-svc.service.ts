import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {  IS_API, HTTP_OPTIONS,ITEM_LIST_URL } from './../../common/url-config';


@Injectable(
//  {  providedIn: 'root'}
)
export class MessageSvcService {

    constructor(private httpClient: HttpClient) {

    }
    errorHandler(error: HttpErrorResponse) {
      console.log(JSON.stringify(error));
      return throwError("Server error ...");
    }
    /** Api hit for Edit Instances **/
    getItemList(itemId): Observable<any[]> {

      console.log("item List Url", ITEM_LIST_URL);
      if (IS_API) {
        return this.httpClient.get<any[]>(ITEM_LIST_URL , HTTP_OPTIONS)
          .pipe(
            catchError(this.errorHandler)
          );
      } else {
        return this.httpClient.get<any[]>(ITEM_LIST_URL).pipe(
          catchError(this.errorHandler)
        );
      }
    }
    //
}
