import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const BASE_URL =  environment.baseUrl;
const LIVE_URL_PATH = BASE_URL+'api/';
const LOCAL_URL_PATH = BASE_URL+'assets/localdata/';


export const IS_API = false;
//export const IS_API = true;


/*Http Options Section */
export const HTTP_OPTIONS = {
        headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
        })
};
/*Http Options Section */


/** Cost Item List URLs **/
const itemListLiveUrl = LIVE_URL_PATH + 'items';
const itemListLocalUrl = LOCAL_URL_PATH + 'items.json';
export const ITEM_LIST_URL = IS_API ? itemListLiveUrl : itemListLocalUrl;


/** Cost Item List URLs **/
