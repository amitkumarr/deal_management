import { Injectable,ErrorHandler, Injector,NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable(
//  {  providedIn: 'root'}
)
/*export class ErrorHandlerService  {

  constructor() { }
}*/
export class ErrorHandlerService implements ErrorHandler {
  errorObject = { "code":500,"message":"Server Error", "exception":"ServerException"};

  constructor(private injector: Injector,private zone:NgZone) { }

  handleError(error: any) {
    let router = this.injector.get(Router);

    console.log('URL: ' + router.url);
    let errorType = error instanceof HttpErrorResponse;
    if (errorType && error.status != 404) {
        this.setErrorDescription(error);
        this.zone.run(() => router.navigate(['error', this.errorObject]));

    } else {
        //A client-side or network error occurred.
        console.log('An error occurred:', error.message);
    }
    // to avoid error : Navigation triggered outside Angular zone, did you forget to call ‘ngZone.run()’?

  }

  setErrorDescription(Error){
      //Backend returns unsuccessful response codes such as 404, 500 etc.
        this.errorObject.code = Error.status;
        this.errorObject.exception = Error.name;
        this.errorObject.message = Error.statusText;

        console.log('Backend returned status code: ', this.errorObject.code);
        console.log('Backend returned exception code: ', this.errorObject.exception);
        console.log('Backend returned message : ', this.errorObject.message);


    }
}
