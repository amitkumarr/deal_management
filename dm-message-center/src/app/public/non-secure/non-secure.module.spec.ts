import { NonSecureModule } from './non-secure.module';

describe('NonSecureModule', () => {
  let nonSecureModule: NonSecureModule;

  beforeEach(() => {
    nonSecureModule = new NonSecureModule();
  });

  it('should create an instance', () => {
    expect(nonSecureModule).toBeTruthy();
  });
});
