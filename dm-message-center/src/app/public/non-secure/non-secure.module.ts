import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { PublicModuleRouting } from './public-routing';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PublicModuleRouting
  ],
  declarations: [AboutComponent]
})
export class NonSecureModule { }
