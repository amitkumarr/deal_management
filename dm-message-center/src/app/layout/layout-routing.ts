import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { SecureLayoutComponent } from './secure-layout/secure-layout.component';
import { SecureNavigationMenuComponent } from './secure-navigation-menu/secure-navigation-menu.component';
import { MessageBoxModule } from '../secure/messagebox/messagebox.module';
import { NonSecureModule } from '../public/non-secure/non-secure.module';



const routes: Routes = [
   {   path: '',
     component: SecureLayoutComponent,
     children: [
       { path: '',  redirectTo: '/messagecenter',  pathMatch: 'full'  },
       { path: 'messagecenter', loadChildren: ()=>MessageBoxModule }

     ]
  }



];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})


export class LayoutRouting { }
